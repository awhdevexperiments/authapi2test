const {ObjectID} = require('mongodb');
const jwt = require('jsonwebtoken');

const {User} = require('./../../models/user');
const {Permission} = require('./../../models/permission');
const {Role} = require('./../../models/role');
const {Question} = require('./../../models/question');
const {Case} = require('./../../models/case');
const {Form} = require('./../../models/form');
const {Organization} = require('./../../models/organization');

const userOneId = new ObjectID();
const userTwoId = new ObjectID();
const userThreeId = new ObjectID();

const users = [{
    _id: userOneId,
    username: 'b.kristhian.tiu@gmail.com',
    first_name: 'Kristhian',
    last_name: 'Tiu',
    middle_name: 'Briones',
    password: 'userOnePass',
    isDeleted: false,
    roles: ['Admin'],
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userOneId, access: 'auth'}, process.env.JWT_SECRET).toString()
    }]
}, {
    _id: userTwoId,
    username: 'b.kristhian.tiu2@gmail.com',
    first_name: 'Chan',
    last_name: 'Tiu',
    middle_name: 'Briones',
    password: 'userTwoPass',
    isDeleted: false,
    roles: [],
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userTwoId, access: 'auth'}, process.env.JWT_SECRET).toString()
    }],
},{
    _id: userThreeId,
    username: 'angelobriones@gmail.com',
    first_name: 'Angelo',
    last_name: 'Briones',
    middle_name: 'Penalosa',
    password: 'userThreePass',
    isDeleted: true,
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userThreeId, access: 'auth'}, process.env.JWT_SECRET).toString()
    }]
}];

const permissions = [{
    _id: new ObjectID(),
    perm_code: 'add_user',
    application: 'Admin',
    describe: 'can add a new user'
}, {
    _id: new ObjectID(),
    perm_code: 'delete_user',
    application: 'Admin',
    describe: 'can delete a new user'
}];

const roles = [{
    _id: new ObjectID(),
    rolename: 'Admin',
    description: 'Administrator role',
    permissions: ['add_user', 'delete_user'],
    isActive: true
}, {
    _id: new ObjectID(),
    rolename: 'Guest',
    description: 'Guest role',
    permissions: [],
    isActive: false
}];

const organizations = [{
    _id: new ObjectID(),
    "name": "SEAOIL",
    "isDeleted": false
},
{
    _id: new ObjectID(),
    "name": "BEARBRAND",
    "isDeleted": true
}];

const populateUsers = (done) => {
    User.remove({}).then(() => {
        var userOne = new User(users[0]).save();
        var userTwo = new User(users[1]).save();
        var userThree = new User(users[2]).save();
        return Promise.all([userOne, userTwo, userThree])
    }).then(() => done());
};

const populatePermissions = (done) => {
    Permission.remove({}).then(() => {
        var permissionOne = new Permission(permissions[0]).save();
        var permissionTwo = new Permission(permissions[1]).save();
        return Promise.all([permissionOne, permissionTwo])
    }).then(() => done());
};

const populateRoles = (done) => {
    Role.remove({}).then(() => {
        var roleOne = new Role(roles[0]).save();
        var roleTwo = new Role(roles[1]).save();
        return Promise.all([roleOne, roleTwo])
    }).then(() => done());
};

const populateOrganizations = (done) => {
    Organization.remove({}).then(() => {
        var org1 = new Organization(organizations[0]).save();
        var org2 = new Organization(organizations[1]).save();
        //var org3 = new Organization(organizations[2]).save();
        return Promise.all([org1, org2]);
    }).then(() => done());
};

module.exports = {
    users,
    populateUsers,
    permissions,
    populatePermissions,
    roles,
    populateRoles,
    organizations,
    populateOrganizations
} 